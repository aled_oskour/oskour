"""Oskour URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
import main.views as views
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('login', views.connexion, name='cas_ng_login'),
    path('logout', views.deconnexion, name='cas_ng_logout'),
    path('oubliMotdePasse', views.oubliMDP, name='oubliMDP'),
    path('creationCompte', views.creation_compte, name='creationCompte'),
    path('changementMotdePasse', views.changePassword, name='changePassword'),
    path('ouvertureDossier', views.ouverture_dossier, name='ouvertureDossier'),
    path('liste_matieres_dossier', views.get_liste_matieres_ajout_dossier, name='listeAjoutDossier'),
    path('add_matiere_dossier', views.ajout_matiere_dossier, name='ajoutMatiereDossier'),
    path('suppression_matiere_dossier', views.suppr_matiere_dossier, name='suppressionMatiereDossier'),
    path('parametres', views.parametres, name="parametres"),
    path('cours/<int:cours>', views.cours, name="cours"),
    path('reponses/vote', views.vote, name="upvote"),
    path('cours/supprimer', views.supprimer_question, name="supprimerQuestion"),
    path('cours/desinscription', views.desinscription, name="desinscriptionMatiere"),
    path('cours/favoris', views.favoriser, name="favori"),
    path('cours/epingler', views.epingler, name="epingler"),
    path('cours/recherche', views.recherche, name="recherche"),
    path('cours/like', views.like, name="like"),
    path('selection_matieres', views.get_all_matieres, name='selection'),
    path('cours/selection_matieres', views.get_all_matieres, name='selectionviacours'),
    path('selection_matieres/<int:id_matiere>', views.password_matiere, name='selectionpassword'),
    path('reponses/<int:id_question>', views.reponses, name='reponses'),
    path('reponses/supprimer', views.supprimer_reponse, name='supprimerReponse'),
    path('reponses/valider', views.valider_reponse, name='validerReponse'),
    path('statistique/<int:id_matiere>', views.statistique, name='statistique'),
    path('formulaires_dossiers', views.use_forms_dossier, name='formulairesDossier'),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activation_compte, name='activate'),
    re_path(r'^changeMDP/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.changeMDP, name='changeMDP'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

