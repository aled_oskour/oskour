# Oskour

# Introduction

Le projet Oskour a pour but de permettre une interaction facilitée entre les étudiants et les
professeurs pendant et hors des cours.

Dans cette optique, nous avons choisi de réaliser un site sur lequel les professeurs peuvent créer des
matières auxquelles les étudiants pourront s’inscrire et poser leurs questions. D’autres étudiants,
inscrits à la matière correspondante peuvent y répondre ainsi que le professeur, qui peut, en plus,
valider une réponse.

## Installation

### Prérequis 

Pour pouvoir utiliser le projet il vous faudra :

 - Python 3
 - PIP 3
 - Mysql (Peux être remplacé par Postgres, SQLite ou Oracle)

### Dépendances

Pour lancer le projet, certaines dépendances sont nécessaires. Il vous faudra entrer la commande suivante :

```sh
$ pip3 install -r requirements.txt
```

**Attention :** La librairie mysqlclient nécessite que certains paquets soient installés sur votre système. Pour les connaître [cliquez ici](https://pypi.org/project/mysqlclient/).

### Mise en place de la base de donnée

Une base de donnée doit être créée ainsi qu'un utilisateur ayant accès à cette base.
Ensuite il faudra modifier le fichier [settings.py](https://gitlab.com/aled_oskour/oskour/-/blob/develop/Oskour/settings.py) à la section "DATABASES"
et changer le paramètre comme tel :

 - **ENGINE** : Pour la base de donnée utilisée, ici nous utilisons mysql donc nous allons mettre 'django.db.backends.mysql'
 - **NAME** : Le nom de la base de donnée que vous avez créé
 - **USER** : Le nom de l'utilisateur qui va accéder à la base
 - **PASSWORD** : Le mot de passe pour accéder à la base
 - **HOST** : L'IP de la base
 - **PORT** : Le port de la base

Une fois la base mise en place il faudra exécuter cette commande à la racine du projet :
```sh
$ python3 manage.py migrate
```

### Mise en place du serveur mail

Pour pouvoir faire la création des utilisateurs, des mails de confirmations seront envoyés. Il faut donc mettre en place un serveur mail.
Nous ne détaillerons pas comment créer un serveur mail mais comment configurer le projet pour qu'il fonctionne avec.
Pour configurer les mails il faudra modifier le fichier [settings.py](https://gitlab.com/aled_oskour/oskour/-/blob/develop/Oskour/settings.py) :

 - **EMAIL_USE_TLS** : Protocole de sécurité, nous conseillons de toujours le mettre à True
 - **EMAIL_HOST** : L'adresse du serveur mail
 - **EMAIL_HOST_USER** : L'adresse mail qui va envoyer les email
 - **EMAIL_HOST_PASSWORD** : Mot de passe de l'adresse email
 - **EMAIL_PORT** : Port du serveur mail

Lorsqu'un utilisateur va essayer de se créer un compte, il sera assigné au statut d'élève ou de professeur en fonction du nom de domaine de son adresse mail.
Pour configurer les domaines il faudra modifier le fichier [settings.py](https://gitlab.com/aled_oskour/oskour/-/blob/develop/Oskour/settings.py) :

 - **EMAIL_DOMAIN_TEACHER** : Nom de domaine pour les professeurs
 - **EMAIL_DOMAIN_STUDENT** : Nom de domaine pour les étudiants

Toute inscription avec un autre nom de domaine que celles renseignées ici seront automatiquement refusées.

### Lancement du serveur

Pour lancer le serveur il suffit de lancer la commande suivante à la racine du projet :

```sh
$ python3 manage.py runserver IP:PORT
```

En remplaçant "IP" par l'ip voulue du serveur et "PORT" par le port voulu.

**Attention** : Le port utilisé doit être renseigné dans les [settings.py](https://gitlab.com/aled_oskour/oskour/-/blob/develop/Oskour/settings.py) dans la section **ALLOWED_HOSTS**

## Contributeurs

* **Lilian Delouvy**
* **Hugo Haquette**
