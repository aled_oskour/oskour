from main.models import Matiere, Profil, Categorie
from django.shortcuts import redirect

def create_sidebar_request(request):
    '''
    Créer la sidebar grâce à une requête
    '''
    if not request.user.is_authenticated:
        return redirect("/")
    sidebar = list()
    profil = Profil.objects.get(utilisateur=request.user)
    dossiers_user = Categorie.objects.filter(createur=request.user)
    for matiere in Matiere.objects.filter(profils=profil):
        sidebar.append((matiere.nom, "/cours/" + str(matiere.id)))
    nav = {("Deconnexion", "/logout"), ("Paramètres", "/parametres")}
    return sidebar, nav, dossiers_user
