from django.contrib.auth.hashers import check_password
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from main.models import Matiere, User, Profil, Categorie
from main.forms import PasswordMatiereForm, NewFolderForm, DeleteFolderForm
from main.views import createSidebar, formsDossiers
from main.views.cours import getLuminance

def get_all_matieres(request):
    #récupère toutes les matières de la base de données
    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    profil = Profil.objects.get(utilisateur=request.user)
    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(request.user, None)

    resultat = list()
    for matiere in Matiere.objects.all():
        if getLuminance(matiere.couleur) > 130:
            couleur_titre = "#000000"
        else:
            couleur_titre = "#FFFFFF"
        resultat.append([matiere, couleur_titre])
    return render(request, "site/selectionMatieres.html", locals())

def password_matiere(request, id_matiere):     #vérifie si la matière a un mot de passe et réagit en conséquence
    formulaires = list()
    profil = Profil.objects.get(utilisateur=request.user)
    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(request.user, None)
    matiere = Matiere.objects.get(id=id_matiere)
    titre_formulaire = matiere.nom

    #vérifie si l'utilisateur n'est pas déjà inscrit
    if profil in matiere.profils.all():
        return redirect('/cours/' + str(matiere.id))
    if matiere.mot_de_passe == "":
        matiere.profils.add(profil)
        return redirect('/cours/' + str(matiere.id))

    form = PasswordMatiereForm(request.POST or None)
    formulaires.append(form)
    form.titre = "Veuillez entrer le mot de passe"

    if form.is_valid() and request.method == "POST":
        mdp = form.cleaned_data['mot_de_passe']
        if matiere.checkPassword(mdp):
            matiere.profils.add(profil)
            return redirect('/cours/' + str(matiere.id))
        messages.error(request, "Le mot de passe entré est incorrect")

    return render(request, "site/parametres.html", locals())
