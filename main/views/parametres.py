from django.shortcuts import render, get_object_or_404, redirect
from django.db import IntegrityError
from django.http import Http404, JsonResponse
from main.forms import ParamUserForm, MatiereForm, NewFolderForm, DeleteFolderForm
from main.models import Matiere, Profil, User, Categorie, Question
from main.views.selectionMatieres import get_all_matieres
from django.utils.crypto import get_random_string
from django import forms
from django.contrib import messages
from django.views.decorators.http import require_POST
from main.views import createSidebar, formsDossiers


def parametresEleve(request):
    ''' Définit le menu de paramétrage '''

    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    formulaires = list()

    # Récupération des données
    profil = Profil.objects.get(utilisateur=request.user)
    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(request.user, None)
    form = ParamUserForm(request.POST or None)
    form.titre = "Paramètres utilisateur"
    form.fields['mode_anonyme'].initial = profil.anonyme

    matieres_eleve = Matiere.objects.filter(profils = profil)
    questions_eleve = Question.objects.filter(createur = request.user)
    is_eleve = True
    tab_questions_par_matiere = list()
    for matiere in matieres_eleve:
        tab_questions = list()
        for question in questions_eleve:
            if matiere == question.matiere:
                tab_questions.append(question)
        tab_questions_par_matiere.append((matiere, tab_questions))

    # Traitement de la requête

    if form.is_valid() and 'mode_anonyme' in form.changed_data:
        profil.anonyme = form.cleaned_data['mode_anonyme']
        # S'il a coché le mode anonyme, on génère un pseudo anonyme et on l'enregistre en base
        if profil.anonyme:
            profil.pseudo_anonyme = get_random_string(length=10)
        profil.save()
        messages.success(request, "Les modifications ont été prises en compte.")
        return redirect("/parametres")

    # Si le profil est anonyme, on affiche le pseudo anonyme
    if profil.anonyme:
        form.fields['pseudo_anonyme'].initial = profil.pseudo_anonyme
    else:
        form.fields['pseudo_anonyme'].widget = forms.HiddenInput()

    formulaires.append(form)
    return render(request, 'site/parametres.html', locals())

def parametresProf(request):
    '''Définit le menu de paramétrage du professeur'''

    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    formulaires = list()
    is_prof = True

    #Récupération des données
    form = MatiereForm(request.POST or None)
    user = request.user
    profil = Profil.objects.get(utilisateur=request.user)

    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(user, None)

    matieres_prof = Matiere.objects.filter(createur=user)
    matieres_prof_abonne = Matiere.objects.filter(profils=profil)
    questions_prof = Question.objects.filter(createur=user)
    
    tab_questions_par_matiere_createur = list()
    for matiere in matieres_prof:
        tab_questions = list()
        for question in questions_prof:
            if matiere == question.matiere:
                tab_questions.append(question)
        tab_questions_par_matiere_createur.append((matiere, tab_questions))
    
    tab_questions_par_matiere = list()
    for matiere in matieres_prof_abonne:
        tab_questions = list()
        for question in questions_prof:
            if matiere == question.matiere:
                tab_questions.append(question)
        tab_questions_par_matiere.append((matiere, tab_questions))

    #Traitement de la requête

    if form.is_valid() and request.method == "POST":
        nom = form.cleaned_data['nom_de_matiere']
        mdp = form.cleaned_data['mot_de_passe']
        couleur_matiere = form.cleaned_data['couleur']
        try:
            matiere = Matiere(nom=nom, mot_de_passe=mdp,
                              couleur=couleur_matiere, createur=user)
            matiere.save()
            matiere.profils.add(profil)
            messages.success(request, "Les modifications ont été prises en compte.")
            # Pour faire en sorte que les champs soient vides après avoir submit une matière
            form = MatiereForm()
            sidebar.append((matiere.nom, "/cours/" + str(matiere.id)))
            
        except IntegrityError:
            messages.error(request, "Le nom de la matière est déjà utilisé !")
    
    form.titre = "Création d'une matière"
    formulaires.append(form)

    return render(request, 'site/parametres.html', locals())

def supprimerMatiere(request):
    accepted = False
    id_matiere = request.POST.get('id')
    matiere = Matiere.objects.get(id=id_matiere)
    if(request.user == matiere.createur):
        matiere.delete()
        accepted = True
    data = {'accepted' : accepted}
    return JsonResponse(data)

def modifierMatiere(request):
    accepted = False
    id_matiere = request.POST.get('id')
    new_password = request.POST.get('password')
    print(new_password)
    matiere = Matiere.objects.get(id=id_matiere)
    if(request.user == matiere.createur):
        matiere.mot_de_passe = new_password
        matiere.save()
        accepted = True
    data = {'accepted' : accepted}
    return JsonResponse(data)
    

def parametres(request):
    '''Appelle le type de paramètres correspondant au
    groupe de l'utilisateur (Eleve ou Professeur)'''
    user = request.user
    titre_formulaire = "Paramètres"
    if user.groups.filter(name='Professeurs').exists():
        if request.is_ajax() and request.method == "POST":
            if(request.POST.get('action') == 'suppression'):
                return supprimerMatiere(request)
            elif(request.POST.get('action') == 'modification'):
                return modifierMatiere(request)
        return parametresProf(request)
    elif user.groups.filter(name='Eleves').exists():
        return parametresEleve(request)
    return Http404()

@require_POST
def desinscription(request):
    '''Permet à un étudiant de se désinscrire d'une matière'''
    profil = Profil.objects.get(utilisateur = request.user)
    id_matiere = request.POST.get('id_matiere')
    matiere = Matiere.objects.get(id=id_matiere)
    matiere.profils.remove(profil)
    accepted = True
    data = {'accepted' : accepted}
    return JsonResponse(data)
