''' Importation des modules nécessaires, des modèles et
de ceux nécessaires pour la creation/activation des comptes '''
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from django.contrib import messages
#creation_compte et activation_compte
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.http import HttpResponseNotFound
from django.views.decorators.http import require_POST
from django.contrib.auth.hashers import make_password

from django.conf import settings
from main.views import createSidebar
from main.forms import ConnexionForm, CreationForm, MDPOublieForm, ChangementMDPForm
from main.models import User

def connexion(request):
    '''Page de connexion et inscription'''

    if request.user.is_authenticated:
        messages.error(request, "Vous êtes déjà connecté !")
        return redirect("/")

    form_connexion = ConnexionForm(request.POST or None)
    createSidebar.create_sidebar_request(request)

    if form_connexion.is_valid():
        mail = form_connexion.cleaned_data['email']
        mdp = form_connexion.cleaned_data['mdp']
        user = authenticate(request, username=mail, password=mdp)
        if user is None:
            messages.error(
                request,
                "Authentification refusée: la combinaison email/mot de passe est invalide"
            )
        else:
            if not user.is_active:
                messages.error(request, "Vous n'avez pas validé votre compte !")
                return redirect("/")
            login(request, user)
            return redirect("/")
    return render(request, 'site/connexion.html', locals())

def deconnexion(request):
    '''Deconnexion de l'utilisateur'''
    logout(request)
    return redirect("/")

def creation_compte(request):
    '''Page de création de compte'''

    if request.user.is_authenticated:
        messages.error(request, "Vous ne pouvez pas créer deux comptes pour une seule adresse mail !")
        return redirect("/")

    nav = {("Connexion", "login")}
    form_creation = CreationForm(request.POST or None)

    if form_creation.is_valid():
        mail = form_creation.cleaned_data['email']
        mdp1 = form_creation.cleaned_data['mdp1']
        mdp2 = form_creation.cleaned_data['mdp2']

        if mdp1 == mdp2:
            testuser = User.objects.filter(email=mail)
            if testuser:
                messages.error(request, "Cette adresse email est déjà utilisée.")
            else:
                try:
                    nom_prenom = mail.split("@")[0]
                    identifiant_universite = mail.split("@")[1]
                    prenom = nom_prenom.split(".")[0]
                    nom = nom_prenom.split(".")[1]
                    username = " ".join([prenom, nom])
                except IndexError:
                    messages.error(
                        request,
                        "Le format de l'adresse email (prenom.nom@domaine) est invalide."
                    )
                    return redirect("/creationCompte")
                if identifiant_universite == settings.EMAIL_DOMAIN_TEACHER: #cas d'un professeur
                    groupe = Group.objects.get(name="Professeurs")
                    user = User.objects.create_user(
                        username=username,
                        first_name=prenom,
                        last_name=nom,
                        email=mail,
                        password=mdp1
                    )
                    user.groups.add(groupe)
                elif identifiant_universite == settings.EMAIL_DOMAIN_STUDENT:
                    groupe = Group.objects.get(name="Eleves")
                    user = User.objects.create_user(
                        username=username,
                        first_name=prenom,
                        last_name=nom,
                        email=mail,
                        password=mdp1
                    )
                    user.groups.add(groupe)
                else:
                    messages.error(request, "L'adresse email entrée est invalide.")
                    return redirect("/creationCompte")
                #ajout d'une vérification par mail
                domaine = get_current_site(request)
                email_subject = 'Activez votre compte sur Oskour'
                message = render_to_string('site/messageAuto.html', {
                    'username' :  username,
                    'domain' : domaine.domain,
                    'uid' : urlsafe_base64_encode(force_bytes(user.pk)),
                    'token' : PasswordResetTokenGenerator().make_token(user),
                })
                to_email = mail
                email = EmailMessage(email_subject, message, to=[to_email])
                email.send()
                messages.success(request, "Un email de confirmation vous a été envoyé.")
                #Fin de la vérification par mail
                user.is_active = False
                user.save()
                return redirect("/")
        else:
            messages.error(request, "Les mots de passe entrés ne sont pas les mêmes !")

    return render(request, 'site/creationCompte.html', locals())

def activation_compte(request, uidb64, token):
    ''' Activation du compte lors du clic sur l'url envoyée par mail '''
    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, User.DoesNotExist):
        user = None
    if user is not None and PasswordResetTokenGenerator().check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(request, "Votre compte a été validé !")
        return redirect("/")
    return HttpResponseNotFound(
        "<h1>Houston, on a un problème !</h1></br>"+
        "<h4>L'activation du compte a échoué."+
        " Veuillez contacter l'administrateur du site afin d'obtenir de l'aide.</h4>"
        )

def oubliMDP(request):
    '''Page d'oubli des mots de passe '''
    
    if request.user.is_authenticated:
        messages.error(request, "Vous êtes déjà connecté !")
        return redirect("/")

    nav = {("Connexion", "login")}
    form_oubli = MDPOublieForm(request.POST or None)

    if form_oubli.is_valid():
        try:
            mail = form_oubli.cleaned_data['email_oubli']
            nom_prenom = mail.split("@")[0]
            prenom = nom_prenom.split(".")[0]
            nom = nom_prenom.split(".")[1]
            username = " ".join([prenom, nom])

            user = User.objects.get(email=mail)
            if not user:
                messages.error(request, "Cette adresse email ne correspond à aucun compte connu")
                return redirect('/oubliMotdePasse')

            domaine = get_current_site(request)
            email_subject = 'Nouveau mot de passe sur Oskour'
            message = render_to_string('site/messageMDPAuto.html', {
                'username' :  username,
                'domain' : domaine.domain,
                'uid' : urlsafe_base64_encode(force_bytes(user.pk)),
                'token' : PasswordResetTokenGenerator().make_token(user),
            })
            to_email = mail
            email = EmailMessage(email_subject, message, to=[to_email])
            email.send()
            return redirect("/")
        except IndexError:
            messages.error(
                request,
                "Le format de l'adresse email (prenom.nom@domaine) est invalide."
            )
            return redirect("/oubliMotdePasse")

    return render(request, 'site/oubliMotdePasse.html', locals())

def changeMDP(request, uidb64, token):
    '''Page de changement du mot de passe'''

    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, User.DoesNotExist):
        user = None

    if user is not None and PasswordResetTokenGenerator().check_token(user, token):

        form_changement = ChangementMDPForm(None)
        form_changement.fields['uid'].initial = uidb64
        form_changement.fields['token'].initial = token
        return render(request, 'site/changementMotdePasse.html', locals())
    
    return HttpResponseNotFound(
        "<h1>Houston, on a un problème !</h1></br>"+
        "<h4>Le changement du mot de passe a échoué."+
        " Veuillez contacter l'administrateur du site afin d'obtenir de l'aide.</h4>"
        )

@require_POST
def changePassword(request):
    form_changement = ChangementMDPForm(request.POST or None)

    if form_changement.is_valid():
        uidb64 = form_changement.cleaned_data['uid']
        token = form_changement.cleaned_data['token']
        try:
            uid = force_bytes(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, User.DoesNotExist):
            user = None

        if user is not None and PasswordResetTokenGenerator().check_token(user, token):
            nouveau_mdp1 = form_changement.cleaned_data['nouveau_mdp1']
            nouveau_mdp2 = form_changement.cleaned_data['nouveau_mdp2']

            if nouveau_mdp1 == nouveau_mdp2:
                user.password = make_password(nouveau_mdp1)
                user.save()
                messages.success(request, "Le mot de passe de votre compte a été changé.")
                return redirect("/")
            else:
                messages.error(request, "Les mots de passe entrés ne sont pas les mêmes !")
            
            return redirect("/changeMDP/" + uidb64 + "/" + token)
    return redirect("/")
