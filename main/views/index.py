from django.shortcuts import render, redirect
from django.http import JsonResponse
from main.forms import NewFolderForm, DeleteFolderForm
from main.models import Matiere, Profil, Categorie
from main.views import createSidebar
from django.views.decorators.http import require_POST

def index(request):
    ''' Page d'accueil du site
    Si l'utilisateur est connecté, on affiche la page adequate'''
    if not request.user.is_authenticated:
        nav = {("Connexion", "login")}
        return render(request, 'site/index.html', locals())
    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    profil = Profil.objects.get(utilisateur=request.user)
    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(request.user, None)
    return render(request, 'site/index.html', locals())

@require_POST
def ouverture_dossier(request):
    id_dossier = request.POST.get('id')
    dossier = Categorie.objects.get(id=id_dossier, createur=request.user)
    matieres_dossier = dossier.matieres
    infos_matieres = list()
    for matiere in matieres_dossier.all():
        infos_matieres.append((matiere.nom, matiere.id))
    data = {'infos_matieres': infos_matieres}
    return JsonResponse(data)

@require_POST
def suppr_matiere_dossier(request):
    id_matiere = request.POST.get('idMatiere')
    id_dossier = request.POST.get('idDossier')
    dossier = Categorie.objects.get(id=id_dossier, createur=request.user)
    matiere = Matiere.objects.get(id=id_matiere)
    dossier.matieres.remove(matiere)
    is_removed = True
    data = {'isRemoved': is_removed }
    return JsonResponse(data)

@require_POST
def get_liste_matieres_ajout_dossier(request):
    profil = Profil.objects.get(utilisateur=request.user)
    id_dossier = request.POST.get('idDossier')
    dossier = Categorie.objects.get(id=id_dossier)
    matieres_dossier = dossier.matieres
    all_matieres = Matiere.objects.filter(profils=profil)
    infos_matieres = list()
    for matiere in all_matieres.all():
        accepted = True
        for matiere_exclue in matieres_dossier.all():
            if matiere.id == matiere_exclue.id:
                accepted = False
        if accepted:
            infos_matieres.append((matiere.nom, matiere.id))
    data = {'infos_matieres': infos_matieres}
    return JsonResponse(data)

@require_POST
def ajout_matiere_dossier(request):
    id_dossier = request.POST.get('idDossier')
    id_matiere = request.POST.get('idMatiere')
    dossier = Categorie.objects.get(id=id_dossier)
    matiere = Matiere.objects.get(id=id_matiere)
    dossier.matieres.add(matiere)
    is_accepted = True
    data = {'isAccepted' : is_accepted}
    return JsonResponse(data)