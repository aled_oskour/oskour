from django.contrib import messages
from django.shortcuts import redirect
from main.forms import NewFolderForm, DeleteFolderForm
from main.models import Categorie, Profil

def use_forms_dossier(request):

    profil = Profil.objects.get(utilisateur=request.user)
    form_nouveau_dossier = NewFolderForm(profil, request.POST or None)
    form_all_dossiers = DeleteFolderForm(request.user, request.POST or None)

    if form_nouveau_dossier.is_valid():
        nom_dossier = form_nouveau_dossier.cleaned_data['nom_dossier']
        matieres_dossier = form_nouveau_dossier.cleaned_data['choix_matieres']
        nom_existant = Categorie.objects.filter(createur = request.user, nom=nom_dossier)
        if not nom_existant:
            nouveau_dossier = Categorie(nom=nom_dossier, createur = request.user)
            nouveau_dossier.save()
            for matiere in matieres_dossier:
                nouveau_dossier.matieres.add(matiere)
            form_nouveau_dossier = NewFolderForm(profil)
            messages.success(request, "Le dossier a été créé.")
        else:
            messages.error(request, "Un dossier du même nom existe déjà!")
    else:
        form_nouveau_dossier = NewFolderForm(profil)
   
    if form_all_dossiers.is_valid():
        count = 0
        dossiers_choisis = form_all_dossiers.cleaned_data['choix_dossiers']
        for dossier in dossiers_choisis:
            count +=1
            dossier.delete()
        if count == 1:
            messages.error(request, "Le dossier a été supprimé.")
        else:
            messages.error(request, "Les dossiers ont été supprimés.")
        form_all_dossiers = DeleteFolderForm(request.user)
    else:
        form_all_dossiers = DeleteFolderForm(request.user)

    return redirect(request.META['HTTP_REFERER'])