from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import Group
from main.models import Matiere, User, Question, Votes, Profil, Categorie
from main.views import createSidebar, formsDossiers
import colorsys
from main.forms import QuestionForm, NewFolderForm, DeleteFolderForm
from django.contrib import messages
from django.db import IntegrityError
from django.core.paginator import Paginator
from django.http import JsonResponse, Http404
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from django.views.decorators.http import require_POST, require_GET
from PIL import Image

def getLuminance(rgb):
    '''
    Permet de récupérer la luminance avec un code rgb
    '''
    rgb = rgb.lstrip('#')
    couleur = tuple(int(rgb[i:i+2], 16) for i in (0, 2, 4))
    hls = colorsys.rgb_to_hls(couleur[0], couleur[1], couleur[2])
    return hls[1]

def cours(request, cours):
    ''' Page des question/réponse
    Si l'utilisateur n'est pas connecté, on le redirige vers la page d'acceuil
    '''
    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    user = request.user
    profil = Profil.objects.get(utilisateur=user)
    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(request.user, None)
    try:
        matiere = Matiere.objects.get(id=cours, profils=profil)
    except ObjectDoesNotExist:
        raise Http404

    form_question = QuestionForm(request.POST or None, request.FILES or None)
    #Traitement de la requête
    
    if form_question.is_valid():
        intitule = form_question.cleaned_data['question']
        texte = form_question.cleaned_data['description']
        image_field = form_question.cleaned_data['image']
        try:
            Question.objects.create(intitule=intitule, texte=texte, createur=user, matiere=matiere, image=image_field)
            form_question = QuestionForm()
            messages.success(request, "La question a été créée.")
        except IntegrityError:
            messages.warning(request, "Une question avec cet intitulé a déjà été posée.")
        except Exception:
            messages.error(request, "Une erreur s'est produite lors de la création de la question.")

    question_list = Question.objects.select_related('matiere').filter(matiere=matiere).order_by('-date_creation')
    question_populaire = question_list.annotate(count_like=Count('like'), count_reponse=Count('reponse')).order_by('-count_like', '-count_reponse')[:10]
    
    questions_favorites = profil.question_set.all()
    
    question_valide_list = question_list.exclude(valide=None).order_by('-valide')
    
    questions_epinglees = question_list.filter(epingle=True)
    paginator = Paginator(question_list, 10)
    paginator_valide = Paginator(question_valide_list, 10)
    page = request.GET.get('page')
    page_valide = request.GET.get('page_valide')
    questions = paginator.get_page(page)
    questions_valide = paginator_valide.get_page(page_valide)
    color = matiere.couleur
    luminance = getLuminance(color)
    if user == matiere.createur:
        possibilite_modifier = True
    else:
        possibilite_modifier = False
    if luminance > 128:
        couleur_titre = "#000000"
    else:
        couleur_titre = "#FFFFFF"
    nom_cours = matiere.nom
    return render(request, "site/cours.html", locals())

@require_POST
def epingler(request):
    '''Permet d'épingler ou de désépingler une question'''
    isCreateur = False  #par défaut, on considère que ce n'est pas le créateur de la matière
    id_question = request.POST.get('id')
    question = Question.objects.get(id=id_question)
    matiere = question.matiere
    if request.user == matiere.createur:
        isCreateur = True
        if not question.epingle:
            question.epingle = True
        else:
            question.epingle = False    #question déjà épinglée: on souhaite la retirer des questions épinglées
        question.save()
    else:
        question.epingle = False
    data = {'isEpingle': question.epingle, 'isCreateur' : isCreateur, 'intitule' : question.intitule}
    return JsonResponse(data)

@require_POST
def favoriser(request):
    '''Permet de mettre une question dans les favoris'''
    profil = Profil.objects.get(utilisateur=request.user)
    id_question = request.POST.get('id')
    question = Question.objects.get(id=id_question)
    matiere = question.matiere
    if matiere.profils.filter(utilisateur=request.user).count() == 0:
        raise Http404
    if profil not in question.profils_favoris.all():
        question.profils_favoris.add(profil)
        is_favori = True
    else:
        question.profils_favoris.remove(profil)
        is_favori = False

    data = {'isFavori': is_favori, 'intitule': question.intitule}
    return JsonResponse(data)

@require_POST
def supprimer_question(request):
    '''Permet de supprimer une question'''
    accepted = False
    id_question = request.POST.get('id')
    question = Question.objects.get(id=id_question)
    if question.matiere.createur == request.user:
        question.delete()
        accepted = True
    data = {'accepted' : accepted}
    return JsonResponse(data)

@require_POST
def desinscription(request):
    '''Permet de se désinscrire d'une matière'''
    accepted = False
    id_matiere = request.POST.get('id')
    matiere = Matiere.objects.get(id=id_matiere)
    profil = Profil.objects.get(utilisateur=request.user)
    matiere.profils.remove(profil)
    accepted = True
    data = {'accepted' : accepted}
    return JsonResponse(data)

@require_GET
def recherche(request):
    ''' Permet de chercher une question parmis toute les questions de la matière'''
    matiere_id = request.GET.get('matiere')
    matiere = Matiere.objects.get(id=matiere_id)
    if matiere.profils.filter(utilisateur=request.user).count() == 0:
        raise Http404
    texte = request.GET.get('q')
    questions = Question.objects.filter(intitule__icontains=texte, matiere=matiere)
    data = list()
    for question in questions:
        data.append({'id' : question.id, 'text' : question.intitule,
                     'href' : "/reponses/" + str(question.id)})
    return JsonResponse(data, safe=False)

@require_POST
def like(request):
    '''
    Permet de like une question en fonction de la requête émise
    '''
    id_question = request.POST.get('id')
    utilisateur = request.user
    question = Question.objects.get(id=id_question)
    matiere = question.matiere
    if matiere.profils.filter(utilisateur=utilisateur).count() == 0:
        raise Http404
    try:
        # Vérifie s'il y a déjà une entrée en BDD
        Question.objects.get(id=id_question, like=utilisateur)
        question.like.remove(utilisateur)
        etat = False
    except ObjectDoesNotExist:
        # Si la personne n'a pas encore like
        question.like.add(utilisateur)
        etat = True
    question = Question.objects.get(id=id_question)
    data = {'like': etat, 'increment': question.like.count()}
    return JsonResponse(data)
