from main.views import createSidebar, formsDossiers
from django.shortcuts import render, redirect
from main.models import Question, Reponse, Commentaire, Votes, Matiere, Profil, Categorie
from main.forms import ReponseForm, CommentaireForm, TextQuestionForm, TextReponseForm, NewFolderForm, DeleteFolderForm
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, Http404
from django.db.models import F
from django.utils import timezone
from django.views.decorators.http import require_POST

def reponses(request, id_question):
    '''
    Affiche les réponses de la question passé en paramètre
    '''
    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    question = Question.objects.get(id=id_question)
    reponses = Reponse.objects.filter(question=question).order_by('-valide', '-increment', 'date_creation')
    reponses_user = Reponse.objects.filter(createur=request.user)
    profil = Profil.objects.get(utilisateur=request.user)

    if request.user == question.createur:
        changer_texte_question = True
    else:
        changer_texte_question = False

    form_reponse = ReponseForm(request.POST or None)
    form_commentaire = CommentaireForm(request.POST or None)
    form_textQuestion = TextQuestionForm(request.POST or None)
    form_textReponse = TextReponseForm(request.POST or None)
    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(request.user, None)
    votes = Votes.objects.filter(utilisateur=request.user)
    matiere = question.matiere

    if matiere.profils.filter(utilisateur=request.user).count() == 0:
        raise Http404

    if form_reponse.is_valid():
        reponse = Reponse()
        reponse.texte = form_reponse.cleaned_data['reponse']
        reponse.question = question
        reponse.createur = request.user
        reponse.save()
        messages.success(request, "La réponse a été publiée.")
        return redirect("/reponses/" + str(id_question))

    if form_commentaire.is_valid():
        commentaire = Commentaire()
        rep = Reponse.objects.get(id=form_commentaire.cleaned_data['id_reponse'])
        commentaire.texte = form_commentaire.cleaned_data['commentaire']
        commentaire.reponse = rep
        commentaire.createur = request.user
        commentaire.save()
        messages.success(request, "Le commentaire a été publié.")
        return redirect("/reponses/" + str(id_question))

    if form_textQuestion.is_valid():
        newText = form_textQuestion.cleaned_data['nouveau_texte_question']
        question.texte = question.texte + "\nEDIT: "
        question.texte += newText
        question.save()
        messages.success(request, "Le texte de la question a été changé.")
        return redirect("/reponses/" + str(id_question))

    if form_textReponse.is_valid():
        newText = form_textReponse.cleaned_data['nouveau_texte_reponse']
        reponse = Reponse.objects.get(id=form_textReponse.cleaned_data['id_reponse_texte'])
        reponse.texte = reponse.texte + "\nEDIT: "
        reponse.texte += newText
        reponse.save()
        messages.success(request, "Le texte de la réponse a été changé.")
        return redirect("/reponses/" + str(id_question))

    if request.user == matiere.createur:
        possibilite_modifier = True
    else:
        possibilite_modifier = False

    return render(request, "site/reponses.html", locals())

@require_POST
def vote(request):
    '''
    Permet d'ajouter ou d'enlever un vote en fonction de la requête émise
    '''
    id_reponse = request.POST.get('id')
    reponse = Reponse.objects.get(id=id_reponse)
    is_up = request.POST.get('up_down')
    utilisateur = request.user
    matiere = reponse.question.matiere

    if matiere.profils.filter(utilisateur=request.user).count() == 0:
        raise Http404
    try:
        vote = Votes.objects.get(reponse=reponse, utilisateur=utilisateur)
        # Si on upvote alors que c'est downvote on ajoute 2 au compteur
        if is_up == 'true' and not vote.is_upvote:
            increment = 2
            vote.is_upvote = True
            vote.save()
        # Si on downvote alors que c'est upvote en enlève 2 au compteur
        elif is_up == 'false' and vote.is_upvote:
            increment = -2
            vote.is_upvote = False
            vote.save()
        # Si on reclique sur le upvote alors qu'on est sur le upvote
        elif is_up == 'true' and vote.is_upvote:
            increment = -1
            vote.delete()
        # Si on reclique sur downvote alors qu'on est sur le downvote
        elif is_up == 'false' and not vote.is_upvote:
            increment = 1
            vote.delete()
        Reponse.objects.filter(id=reponse.id).update(increment=F('increment') + increment)
        
    except ObjectDoesNotExist:
        # Si la personne n'a pas encore voté
        vote = Votes(reponse=reponse, utilisateur=utilisateur)
        # S'il veut upvote on ajoute un
        if is_up == 'true':
            increment = 1
            vote.is_upvote = True
        # S'il veut downvote on enlève un
        else:
            increment = -1
            vote.is_upvote = False
        vote.save()
        Reponse.objects.filter(id=reponse.id).update(increment=F('increment') + increment)
    reponse = Reponse.objects.get(id=id_reponse)
    data = {'increment': reponse.increment}
    return JsonResponse(data)

@require_POST
def supprimer_reponse(request):
    '''Supprime une réponse'''
    accepted = False
    id_reponse = request.POST.get('id')
    reponse = Reponse.objects.get(id=id_reponse)
    question = reponse.question
    matiere = question.matiere
    if request.user == matiere.createur:
        reponse.delete()
        accepted = True
    data = {'accepted' : accepted}
    return JsonResponse(data)

@require_POST
def valider_reponse(request):
    '''Supprime une réponse'''
    accepted = False
    id_reponse = request.POST.get('id')
    reponse = Reponse.objects.get(id=id_reponse)
    question = reponse.question
    matiere = question.matiere
    if request.user == matiere.createur:
        if reponse.valide:
            reponse.valide = False
            question.valide = None
        else:
            reponse.valide = True
            question.valide = timezone.now()
        reponse.save()
        question.save()
        accepted = True
    data = {'accepted' : accepted}
    return JsonResponse(data)