from datetime import timedelta
from django.shortcuts import render
from django.utils import timezone
from main.views import createSidebar
from main.models import Matiere, Question, Reponse, Commentaire, Profil
from main.forms import NewFolderForm, DeleteFolderForm

def get_activity(q_sem, r_sem, c_sem):

    #initialisation des valeurs

    current = timezone.now()
    current = current.day
    result_day = list()

    values_days_list = list()
    all_values_last_week = q_sem.values_list('date_creation', flat=True)
    for value in all_values_last_week:
        values_days_list.append(value)
    all_values_last_week = r_sem.values_list('date_creation', flat=True)
    for value in all_values_last_week:
        values_days_list.append(value)
    all_values_last_week = c_sem.values_list('date_creation', flat=True)
    for value in all_values_last_week:
        values_days_list.append(value)
    values_days_list.sort(reverse=True)

    for i in range(0, 7):
        result_day.append([i, 0])

    #traitement
    
    for element in values_days_list:
        for i in range(0, 7):
            if element.day == (current - i):
                temp_value = (result_day[i][1]) +1
                result_day[i] = [i, temp_value]

    return result_day

def statistique(request, id_matiere):
    sidebar, nav, dossiers_user = createSidebar.create_sidebar_request(request)
    profil = Profil.objects.get(utilisateur=request.user)
    form_nouveau_dossier = NewFolderForm(profil, None)
    form_all_dossiers = DeleteFolderForm(request.user, None)
    matiere = Matiere.objects.get(id=id_matiere)
    questions = Question.objects.filter(matiere=matiere)
    reponses = Reponse.objects.filter(question__in=questions)
    commentaires = Commentaire.objects.filter(reponse__in=reponses)
    count_etu = len(matiere.profils.all())
    count_questions = 0
    count_reponses = 0
    count_commentaires = 0
    count_valide = 0

    moment = timezone.now()
    last_week = moment - timedelta(days=7)
    last_month = moment - timedelta(days=30)

    q_sem = questions.filter(date_creation__gte=last_week)
    q_month = questions.filter(date_creation__gte=last_month)
    r_sem = reponses.filter(date_creation__gte=last_week)
    r_month = reponses.filter(date_creation__gte=last_month)
    c_sem = commentaires.filter(date_creation__gte=last_week)
    c_month = commentaires.filter(date_creation__gte=last_month)

    result_by_day = get_activity(q_sem, r_sem, c_sem)

    popularite_question = list()

    for question in questions:
        increment = 1
        count_questions += 1
        if question.valide:
            count_valide += 1
        reponses_question = Reponse.objects.filter(question=question)
        count = len(reponses_question)
        popularite_question.append([increment, count])
        increment += 1
    count_reponses = len(reponses)
    count_commentaires = len(commentaires)

    count_invalidees = count_questions - count_valide
    ratio_valide = 0
    if count_questions != 0:
        ratio_valide = (count_valide*100)//count_questions
    ratio_invalide = 100-ratio_valide

    count_q_sem = len(q_sem)
    count_q_month = len(q_month)
    count_r_sem = len(r_sem)
    count_r_month = len(r_month)
    count_c_sem = len(c_sem)
    count_c_month = len(c_month)

    count_total_q = len(questions)
    count_total_r = len(reponses)
    count_total_c = len(commentaires)

    return render(request, 'site/statistique.html', locals())
