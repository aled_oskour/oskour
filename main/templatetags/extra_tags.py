from django import template
from django.core.exceptions import ObjectDoesNotExist
from main.models import Commentaire, Question, Reponse, Profil

register = template.Library()

@register.filter
def get_reponse(votes, reponse):
    ''' Permet de récupérer le nombre de vote d'une question'''
    try:
        return votes.get(reponse=reponse)
    except ObjectDoesNotExist:
        return None

@register.filter
def get_commentaires(reponse):
    ''' Permet de récupérer les commentaires d'une question '''
    return Commentaire.objects.filter(reponse=reponse)

@register.filter
def get_real_pseudo(data):
    ''' Permet de récupérer le vrai pseudo de la personne qui à poster la question/réponse/commentaire'''
    return data.createur.username

@register.filter
def is_professeur(utilisateur):
    ''' Permet de savoir si l'utilisateur est un professeur ou non '''
    return utilisateur.groups.filter(name="Professeurs").exists()

@register.filter
def nb_like(question):
    ''' Permet de savoir le nombre de like sur une question '''
    return question.like.count()

@register.filter
def is_like(question, user):
    ''' Permet de savoir si l'utilisateur à like la question '''
    if Question.objects.filter(id=question.id, like=user).count() != 0:
        return True
    return False
