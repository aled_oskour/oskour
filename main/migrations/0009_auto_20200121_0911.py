# Generated by Django 3.0.2 on 2020-01-21 09:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20200120_1603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='intitule',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterUniqueTogether(
            name='question',
            unique_together={('intitule', 'matiere')},
        ),
    ]
