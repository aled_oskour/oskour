function confirmationBox(alertType, parentClass, countId, message){
    var messages = $("li").filter("." + parentClass);
    var count;
    if($(messages).length){
        count = $("#"+countId).parent().val();
        count = count + 1;
        $("#" + countId).parent().remove();
        $("#alertMessage").append("<li class='alert alert-" + alertType + " alert-dismissible fade show " + parentClass + "' role='alert' value='" + count + "'><div id='" + countId + "'>" + message + " (x" + count + ")</div><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></li>");
    }
    else{
        $("#alertMessage").append("<li class='alert alert-" + alertType + " alert-dismissible fade show " + parentClass + "' role='alert' value='1'><div id='" + countId + "'>" + message + "</div><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></li>");
    }
};
