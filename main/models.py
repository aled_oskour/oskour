from django.db import models
from django.contrib.auth.models import User
from colorful.fields import RGBColorField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.hashers import make_password, check_password

class Profil(models.Model):
    utilisateur = models.OneToOneField(User, on_delete=models.CASCADE)
    anonyme = models.BooleanField(default=False)
    pseudo_anonyme = models.CharField(max_length=50, default=None, null=True)

class Matiere(models.Model):
    nom = models.CharField(max_length=50, unique=True)
    mot_de_passe = models.TextField(default=None, null=True)
    couleur = RGBColorField(default='#000000')
    createur = models.ForeignKey(User, on_delete=models.PROTECT)
    profils = models.ManyToManyField(Profil)

    def save(self, *args, **kwargs):    #fonction de chiffrement du mot de passe
        if self.mot_de_passe:
            self.mot_de_passe = make_password(self.mot_de_passe)
        super(Matiere, self).save(*args, **kwargs)

    def checkPassword(self, entryPassword):
        return check_password(entryPassword, self.mot_de_passe)

    def __str__(self):
        return f'{self.nom}'

class Question(models.Model):
    intitule = models.CharField(max_length=50)
    texte = models.TextField()
    date_creation = models.DateTimeField(auto_now_add=True)
    valide = models.DateTimeField(default=None, blank=True, null=True)
    matiere = models.ForeignKey(Matiere, on_delete=models.CASCADE)
    createur = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    profils_favoris = models.ManyToManyField(Profil)
    epingle = models.BooleanField(default=False)
    image = models.ImageField(default=None, null=True, upload_to="image/")
    like = models.ManyToManyField(User, related_name='%(class)s_requests_created')
    pseudo_createur = models.CharField(max_length=50)

    def save(self, *args, **kwargs):
        profil = Profil.objects.get(utilisateur=self.createur)
        if self.createur:
            if profil.anonyme:
                self.pseudo_createur = profil.pseudo_anonyme
            else:
                self.pseudo_createur = self.createur.username
        super(Question, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('intitule', 'matiere')


class Reponse(models.Model):
    texte = models.TextField()
    increment = models.IntegerField(default=0)
    valide = models.BooleanField(default=False)
    date_creation = models.DateTimeField(auto_now_add=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    createur = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    vote = models.ManyToManyField(User, through='Votes', related_name='%(class)s_requests_created')
    pseudo_createur = models.CharField(max_length=50)

    def save(self, *args, **kwargs):
        profil = Profil.objects.get(utilisateur=self.createur)
        if self.createur:
            if profil.anonyme:
                self.pseudo_createur = profil.pseudo_anonyme
            else:
                self.pseudo_createur = self.createur.username
        super(Reponse, self).save(*args, **kwargs)

class Commentaire(models.Model):
    texte = models.TextField()
    date_creation = models.DateTimeField(auto_now_add=True)
    reponse = models.ForeignKey(Reponse, on_delete=models.CASCADE)
    createur = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    pseudo_createur = models.CharField(max_length=50)

    def save(self, *args, **kwargs):
        profil = Profil.objects.get(utilisateur=self.createur)
        if self.createur:
            if profil.anonyme:
                self.pseudo_createur = profil.pseudo_anonyme
            else:
                self.pseudo_createur = self.createur.username
        super(Commentaire, self).save(*args, **kwargs)


class Categorie(models.Model):
    nom = models.CharField(max_length=50)
    matieres = models.ManyToManyField(Matiere)
    createur = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.nom}'

class Votes(models.Model):
    reponse = models.ForeignKey(Reponse, on_delete=models.CASCADE)
    utilisateur = models.ForeignKey(User, on_delete=models.CASCADE)
    is_upvote = models.BooleanField()

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profil.objects.create(utilisateur=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profil.save()
