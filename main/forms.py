from django import forms
from main.models import Matiere, Categorie
from colorful.forms import RGBColorField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class ConnexionForm(forms.Form):
    ''' Formulaire de connexion '''
    email = forms.CharField(required=True, label='', widget=forms.TextInput({ 'placeholder':'Votre adresse Email'}))
    mdp = forms.CharField(required=True, label='', widget=forms.PasswordInput({ 'placeholder': 'Votre mot de passe'}))

    def __init__(self, *args, **kwargs):
        super(ConnexionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/login'
        self.helper.add_input(Submit('submit', 'Connexion', css_class="w-100"))

class MDPOublieForm(forms.Form):
    '''Formulaire de mot de passe oublié'''
    email_oubli = forms.CharField(required=True, label='', widget=forms.TextInput({ 'placeholder':'Votre adresse Email'}))

    def __init__(self, *args, **kwargs):
        super(MDPOublieForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/oubliMotdePasse'
        self.helper.add_input(Submit('submit', 'Envoyer le mail', css_class="w-100"))

class ChangementMDPForm(forms.Form):
    '''Formulaire de changement de mot de passe'''
    nouveau_mdp1 = forms.CharField(required=True, label='', widget=forms.PasswordInput({ 'placeholder': 'Votre nouveau mot de passe'}))
    nouveau_mdp2 = forms.CharField(required=True, label='', widget=forms.PasswordInput({ 'placeholder': 'Veuillez vérifier votre nouveau mot de passe'}))
    uid = forms.CharField(required=True, widget=forms.HiddenInput())
    token = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(ChangementMDPForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/changementMotdePasse'
        self.helper.add_input(Submit('submit', 'Valider', css_class="w-100"))

class CreationForm(forms.Form):
    '''Création d'un compte'''
    email = forms.CharField(required=True, label='', widget=forms.TextInput({ 'placeholder': 'Votre adresse Email'}))
    mdp1 = forms.CharField(required=True, label='', widget=forms.PasswordInput({ 'placeholder': 'Votre mot de passe'}))
    mdp2 = forms.CharField(required=True, label='', widget=forms.PasswordInput({ 'placeholder': 'Veuillez vérifier votre mot de passe'}))

    def __init__(self, *args, **kwargs):
        super(CreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/creationCompte'
        self.helper.add_input(Submit('submit', 'Valider', css_class="w-100"))

class ParamUserForm(forms.Form):
    ''' Formulaire du mode anonyme'''
    pseudo_anonyme = forms.CharField(required=False, disabled=True)
    mode_anonyme = forms.BooleanField(required=False)
    titre = str

    def __init__(self, *args, **kwargs):
        super(ParamUserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/parametres'
        self.helper.add_input(Submit('submit', 'Valider'))

class MatiereForm(forms.Form):
    ''' Formulaire de la création de matière'''
    nom_de_matiere = forms.CharField(max_length=50, label="Nom de la matière", required=True)
    mot_de_passe = forms.CharField(max_length=50, widget=forms.PasswordInput(), required=False)
    couleur = RGBColorField()
    titre = str

    def __init__(self, *args, **kwargs):
        super(MatiereForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/parametres'
        self.helper.add_input(Submit('submit', 'Valider'))

class QuestionForm(forms.Form):
    ''' Formulaire pour poser une nouvelle question '''
    question = forms.CharField(max_length=50, label="Intitulé")
    description = forms.CharField(widget=forms.Textarea, label="Question")
    image = forms.ImageField(label="Image", required=False)

    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ""
        self.helper.add_input(Submit('submit', 'Soumettre la question', css_class='btn-success'))

class PasswordMatiereForm(forms.Form):
    '''Formulaire de saisie du mot de passe de la matière'''
    mot_de_passe = forms.CharField(widget=forms.PasswordInput(), label="Mot de passe")

    def __init__(self, *args, **kwargs):
        super(PasswordMatiereForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/selectionMatieres'
        self.helper.add_input(Submit('submit', 'Valider'))

class ReponseForm(forms.Form):
    ''' Formulaire pour répondre à une question '''
    reponse = forms.CharField(widget=forms.Textarea(attrs={'rows' : 2}), label="Réponse")
    def __init__(self, *args, **kwargs):
        super(ReponseForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.add_input(Submit('submiter', 'Répondre', css_class='btn-primary'))

class CommentaireForm(forms.Form):
    ''' Formulaire pour commenter une réponse '''
    commentaire = forms.CharField(widget=forms.Textarea)
    id_reponse = forms.CharField(widget=forms.HiddenInput())
    def __init__(self, *args, **kwargs):
        super(CommentaireForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ""
        self.helper.add_input(Submit('submit', 'Poster le commentaire', css_class='btn-success'))

class TextQuestionForm(forms.Form):
    '''Formulaire pour changer le texte d'une question'''
    nouveau_texte_question = forms.CharField(widget=forms.Textarea, label="Nouveau texte de la question")
    def __init__(self, *args, **kwargs):
        super(TextQuestionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ""
        self.helper.add_input(Submit('submit', 'Modifier le texte de la question', css_class='btn-success'))

class TextReponseForm(forms.Form):
    '''Formulaire pour changer le texte d'une réponse'''
    nouveau_texte_reponse = forms.CharField(widget=forms.Textarea, label="Nouveau texte de la réponse")
    id_reponse_texte = forms.CharField(widget=forms.HiddenInput())
    def __init__(self, *args, **kwargs):
        super(TextReponseForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ""
        self.helper.add_input(Submit('submit', 'Modifier le texte de la réponse', css_class='btn-success'))

class NewFolderForm(forms.Form):
    '''Formulaire pour créer un nouveau dossier de matières'''
    def __init__(self, profil, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = "/formulaires_dossiers"
        self.helper.add_input(Submit('submit', 'Créer le dossier', css_class='btn-success'))
        super(NewFolderForm, self).__init__(*args, **kwargs)
        self.fields['choix_matieres'].queryset = Matiere.objects.filter(profils=profil)

    nom_dossier = forms.CharField(widget=forms.Textarea(attrs={'rows' : 1}), max_length=50, label="Nom du dossier", required=True)
    choix_matieres = forms.ModelMultipleChoiceField(
        required=False,
        queryset=None,
        widget=forms.CheckboxSelectMultiple,
        label="Ajouter des matières au dossier"
    )

class DeleteFolderForm(forms.Form):
    '''Formulaire de suppression de dossiers'''
    def __init__(self, user, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action ="/formulaires_dossiers"
        self.helper.add_input(Submit('submit', 'Supprimer', css_class='btn-danger'))
        super(DeleteFolderForm, self).__init__(*args, **kwargs)
        self.fields['choix_dossiers'].queryset = Categorie.objects.filter(createur=user)
    
    choix_dossiers = forms.ModelMultipleChoiceField(
        required=True,
        queryset=None,
        widget=forms.CheckboxSelectMultiple,
        label="Supprimer des dossiers"
    )